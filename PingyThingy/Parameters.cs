﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PingyThingy
{
    class Parameters
    {

        [Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
        public bool Verbose { get; set; }
        //[Value(0, Required = true, HelpText = "The ping target",MetaName = "target")]
        [Value(0,Required = true,HelpText = "Target FQDN/IP",MetaName = "target")]
        public String Target { get; set; }
        [Option('l',"length",Default = 32, HelpText = "Packet length", Required = false)]
        public int PacketLength { get; set; }
        [Option("timeout", HelpText = "How long should {0} wait for a connection before timing out", Required = false, Default = 1000)]
        public int TimeOut { get; set; }
        [Option('T',"tcp", HelpText = "Test TCP connectivity", Required = false)]
        public bool TCP { get; set; }
        [Option('p', "port", HelpText = "Which port should {0} connect to", Required = false, Default = 1)]
        public int Port { get; set; }
        [Option('m', "trace", Required = false, HelpText = "ICMP Statistics for hops to target")]
        public bool Trace { get; set; }
        [Option('t', "ttl", Required = false, HelpText = "Maximum TTL for traceroute testing", Default = 32)]
        public int MaxTTL { get; set; }
    }
}
