﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace PingyThingy.Pingers
{
    class TCP : IPinger
    {

        public override void Ping()
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                try
                {
                    var c = tcpClient.BeginConnect(Global.Parameters.Target, Global.Parameters.Port,null,null);
                    if(c.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1)))
                    {
                        State = HostState.ONLINE;
                    }
                }
                catch (Exception)
                {
                    State = HostState.OFFLINE;
                }

            }
            ReportProgress("\r{0} is {1}", Target.Host, State.ToString());
        }
    }
}
