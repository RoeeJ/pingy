﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using Console = Colorful.Console;
namespace PingyThingy.Pingers
{
    public abstract class IPinger
    {
        internal static ManualResetEvent requestTermination = new ManualResetEvent(false);
        internal Thread worker;
        internal double successcount = 0;
        internal double failcount = 0;
        internal long pingsum = 0;
        internal long min = Int32.MaxValue;
        internal long max = Int32.MinValue;
        internal static HostState OldState = HostState.DEFAULT;
        internal static HostState State = HostState.DEFAULT;
        public static List<IPStatus> OfflineStatus = new List<IPStatus>() { IPStatus.TimedOut, IPStatus.DestinationHostUnreachable, IPStatus.DestinationNetworkUnreachable, IPStatus.DestinationPortUnreachable, IPStatus.DestinationProhibited, IPStatus.DestinationProtocolUnreachable, IPStatus.DestinationScopeMismatch, IPStatus.DestinationUnreachable };
        public abstract void Ping();
        internal HostPort Target = Utils.GetTarget();
        internal  int originalCursorTop;
        internal int newCursorTop;

        public virtual void Start()
        {
            PreStart();
            worker = new Thread(() => {
                while (!requestTermination.WaitOne(1000)) {
                    this.Ping();
                }
            });
            worker.Start();
        }
        [SecurityPermissionAttribute(SecurityAction.Demand, ControlThread = true)]
        public virtual void Stop()
        {
            requestTermination.Set();
            if(worker != null) {
                worker.Join();
            } 
            PostStop();
        }
        public virtual void PreStart() { }
        public virtual void PostStop() { }


        public void ReportProgressMulti(List<String> lines)
        {
            originalCursorTop = Console.CursorTop;
            newCursorTop = Console.CursorTop + lines.Count;
            for (int i = lines.Count-1; i >= 0; i--) {
                if (lines[i] == null) continue;
                Console.SetCursorPosition(0, originalCursorTop + i);
                Console.Write("\r{0}", (lines[i]));
            }
            Console.SetCursorPosition(0, originalCursorTop);
        }
        public void ReportProgress(String fmt,params String[] args)
        {
            switch (State)
            {
                case HostState.ONLINE:
                    successcount++;
                    break;
                case HostState.OFFLINE:
                    failcount++;
                    break;
            }
            if (State != OldState)
            {
                if(OldState != HostState.DEFAULT)
                {
                    Console.Write(String.Format("\r{0}: {1} is {2}\n", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), Target.Host, State.ToString()), GetStateColor());
                    Utils.MakeNoise();
                }
                OldState = State;
            }
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
            Console.Write(String.Format(fmt,args), GetStateColor());
        }

        public void UpdateTitle(String fmt,params Object[] args)
        {
            Console.Title = String.Format(fmt, args);
        }

        public Color GetStateColor()
        {
            switch(State)
            {
                case HostState.ERROR:
                    return Color.Yellow;
                case HostState.OFFLINE:
                    return Color.Red;
                case HostState.ONLINE:
                    return Color.Green;
                default:
                    return Color.Blue;

            }
        }
    }
    class HostPort
    {
        public String Host = "";
        public int Port = -1;

        public HostPort(string Host)
        {
            this.Host = Host;
        }

        public HostPort(string Host, short Port)
        {
            this.Host = Host;
            this.Port = Port;
        }

        public override string ToString()
        {
            return Host + ":" + Port;
        }
    }
    public class PingAddress
    {
        public IPAddress Address;
        public PingResult Result;
        public PingStatistic Statistic = new PingStatistic();

        public PingAddress()
        {
        }
        public PingAddress(IPAddress Address)
        {
            this.Address = Address;
        }

        public PingAddress(IPAddress Address, PingResult Result)
        {
            this.Address = Address;
            this.Result = Result;
        }
    }
    public class PingStatistic
    {
        public long Min = Int32.MaxValue;
        public long Max = Int32.MinValue;
        public long Average => CumulativeLatency / Math.Max(1,SuccessCount + FailCount);
        public double LossRatio => 100 - (SuccessCount + FailCount == 0 ? 100 : (SuccessCount > 0 ? (SuccessCount / (SuccessCount + FailCount)) * 100 : 0));
        public long CumulativeLatency = 0;
        public int SuccessCount;
        public int FailCount;
        public PingStatistic() { }
    }
}
