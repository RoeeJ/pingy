﻿namespace PingyThingy.Pingers
{
    public enum HostState
    {
        ONLINE,
        OFFLINE,
        ERROR,
        DEFAULT
    }
}