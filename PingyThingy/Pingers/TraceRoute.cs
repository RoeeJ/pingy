﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PingyThingy.Pingers
{

    class TraceRoute : IPinger
    {
        static PingAddress[] addresses = new PingAddress[Global.Parameters.MaxTTL];
        static int hops;
        public override void PreStart()
        {
            hops = GetTargetHops();
            addresses = GetTargetPath().Result.ToArray();
        }
        public override void Start()
        {
            PreStart();
            while (!requestTermination.WaitOne(1000)) Ping();
        }
        public override void Ping()
        {
            Task.WaitAll(UpdateStatsAsync());
            //Console.Title = (count++).ToString();
            var addys = addresses.Take(hops).Select((pa, i) => {
                if (pa == null || pa.Address.ToString() == "0.0.0.0") {
                    return "======= NO ANSWER =======";
                } else {
                    StringBuilder sb = new StringBuilder();
#if DEBUG
                    sb.AppendFormat("T:{0} Hop {1}: {2}\t\t",pa.Statistic.SuccessCount+pa.Statistic.FailCount, i + 1, pa.Address.ToString().PadRight(30));
#else
                    sb.AppendFormat("Hop {0}: {1}\t\t", i + 1, pa.Address.ToString().PadRight(30));
#endif
                    if (/*(pa.Statistic.Min+pa.Statistic.Max) != 0 ||*/ pa.Statistic.LossRatio != 100) {
                        sb.AppendFormat("L:{0}/H:{1}/A:{2} PL:{3}%", pa.Statistic.Min, pa.Statistic.Max, pa.Statistic.Average, pa.Statistic.LossRatio);
                    }
                    return sb.ToString();
                }
            });
            ReportProgressMulti(addys.ToList());
        }

        private async Task UpdateStatsAsync()
        {
            var results = await Task.WhenAll(Enumerable.Range(0, hops).Select(i => PingHost(addresses[i].Address.ToString())));
            results.ForEachWithIndex((pr, i) => {
                Console.Title = i.ToString();
                if (addresses[i] == null || addresses[i].Address.ToString() == "0.0.0.0") return;
                if (pr.state == HostState.ONLINE) {
                    if (pr.RoundTripTime > addresses[i].Statistic.Max) {   // Check whether the current round-trip time is the new highest
                        addresses[i].Statistic.Max = pr.RoundTripTime;
                    }
                    if (pr.RoundTripTime < addresses[i].Statistic.Min) { // or the new lowest
                        addresses[i].Statistic.Min = pr.RoundTripTime;
                    }
                    addresses[i].Statistic.SuccessCount++;
                    addresses[i].Statistic.CumulativeLatency += pr.RoundTripTime;
                } else {
                    addresses[i].Statistic.FailCount++;
                }
                addresses[i].Result = pr;
            });
        }
        public int GetTargetHops()
        {
            Ping pinger = new Ping();

            for (int ttl = 1; ttl <= Global.Parameters.MaxTTL; ttl++) {
                PingReply reply = pinger.Send(Target.Host, Global.Parameters.TimeOut, new byte[Global.Parameters.PacketLength], new PingOptions(ttl, true));

                if (reply.Status == IPStatus.TtlExpired || reply.Status == IPStatus.TimedOut) {
                    // TtlExpired means we've found an address, but there are more addresses
                    continue;
                }
                if (reply.Status == IPStatus.Success) {
                    // Success means the tracert has completed
                    return ttl;
                }
            }
            return -1;
        }
        public async Task<List<PingAddress>> GetTargetPath() => (await Task.WhenAll(Enumerable.Range(1, hops).Select((i) => GetTraceRoute(Target.Host, i)))).ToList();
        private async Task<PingAddress> GetTraceRoute(string hostNameOrAddress, int ttl)
        {
            PingAddress Address = new PingAddress();
            Ping pinger = new Ping();
            var reply = await pinger.SendPingAsync(hostNameOrAddress, 1000, new byte[Global.Parameters.PacketLength], new PingOptions(ttl, false));
            if (addresses.Any((a) => a != null && a.Address == reply.Address)) return null;
            Address.Address = reply.Address;
            if(addresses[ttl-1] == null) {
                addresses[ttl - 1] = Address;
            }
            if (reply.Status == IPStatus.Success || reply.Status == IPStatus.TtlExpired) {
                Address.Result = new PingResult(HostState.ONLINE, reply.RoundtripTime, Color.Green);
            } else if (OfflineStatus.Contains(reply.Status)) {
                Address.Result = new PingResult(HostState.OFFLINE, -1, Color.Red);
            } else {
                Address.Result = new PingResult(HostState.ERROR, -1, Color.Yellow);
            }
            return Address;
        }
        public async static Task<PingResult> PingHost(string nameOrAddress)
        {
            if (nameOrAddress == "0.0.0.0") return null;
            bool pingable = false;
            Ping pinger = null;
            PingReply reply;
            try {
                pinger = new Ping();
                reply = await pinger.SendPingAsync(nameOrAddress, 1000, new byte[Global.Parameters.PacketLength]);
                pingable = reply.Status == IPStatus.Success;
                if (reply.Status == IPStatus.Success) {
                    //werk
                    return new PingResult(HostState.ONLINE, reply.RoundtripTime, Color.Green);
                } else if (OfflineStatus.Contains(reply.Status)) {
                    return new PingResult(HostState.OFFLINE, -1, Color.Red);
                    //no werk
                } else {
                    Console.WriteLine("ERR:" + reply.Status.ToString(), Color.Yellow);
                    return new PingResult(HostState.ERROR, -1, Color.Yellow);
                    //misc
                }
            } catch (PingException) {
                // Discard PingExceptions and return false;
                return new PingResult(HostState.ERROR, -1, Color.Yellow);
            } finally {
                if (pinger != null) {
                    pinger.Dispose();
                }
            }
        }


    }
}
