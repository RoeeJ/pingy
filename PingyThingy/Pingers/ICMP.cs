﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Drawing;
using Console = Colorful.Console;
namespace PingyThingy.Pingers
{
    public class ICMP : IPinger
    {

        public override void Ping()
        {
            Ping ping = new Ping();
            ping.PingCompleted += Ping_PingCompleted;
            ping.SendAsync(Target.Host, null);
        }

        private void Ping_PingCompleted(object sender, PingCompletedEventArgs e)
        {
            PingReply reply = e.Reply;
            PingResult pr;
            if (reply.Status == IPStatus.Success) {
                //werk
                pr = new PingResult(HostState.ONLINE, reply.RoundtripTime, Color.Green);
            } else if (OfflineStatus.Contains(reply.Status)) {
                pr = new PingResult(HostState.OFFLINE, -1, Color.Red);
                //no werk
            } else {
                Console.WriteLine("ERR:" + reply.Status.ToString(), Color.Yellow);
                pr = new PingResult(HostState.ERROR, -1, Color.Yellow);
                //misc
            }
            State = pr.state;
            if (pr.state == HostState.ONLINE) {
                pingsum += pr.RoundTripTime;
                if (pr.RoundTripTime < min) min = pr.RoundTripTime;
                if (pr.RoundTripTime > max) max = pr.RoundTripTime;
                //Console.Write(String.Format("\r{0} is {1} ({2}ms)"), Color.Green);
            }
            double ratio = successcount + failcount == 0 ? 100 : 100 - Math.Floor((successcount + failcount == 0 ? 100 : (successcount > 0 ? (successcount / (successcount + failcount)) * 100 : 0)));
            ReportProgress(State == HostState.ONLINE ? "\r{0} is {1} ({2}ms)" : "\r{0} is {1}", Target.Host, pr.state.ToString(), pr.RoundTripTime.ToString());
            UpdateTitle("Pingy: {0} - min/max/avg:{1}/{2}/{3} PL:{4:0.##}% len:{5}", Target.Host, ratio == 100 ? "-" : min.ToString(), ratio == 100 ? "-" : max.ToString(), ratio == 100 ? "-" : ((int)(pingsum / (successcount + failcount))).ToString(), (successcount + failcount) == 0 ? 0 : ratio, Global.Parameters.PacketLength);

        }
    }
    public class PingResult
    {
        public HostState state;
        public long RoundTripTime;
        public Color color;
        public PingResult(HostState state, long RoundTripTime, Color color)
        {
            this.state = state;
            this.RoundTripTime = RoundTripTime;
            this.color = color;
        }
    }
}
