﻿using CommandLine;
using Console = Colorful.Console;
using System;
using System.Drawing;
using System.Text;
using CommandLine.Text;
using System.Reflection;
using System.Linq;
using PingyThingy.Pingers;
using System.Net;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PingyThingy
{
    internal class Utils
    {
        #region Native
        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);
        public delegate bool HandlerRoutine(CtrlTypes CtrlType);
        static HandlerRoutine Ctrl_C_Handler = new HandlerRoutine(OnConsoleCtrlEvent);
        // An enumerated type for the control messages
        // sent to the handler routine.
        public enum CtrlTypes
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

        private static bool OnConsoleCtrlEvent(CtrlTypes ctrlType)
        {
            ResetConsole();
            Program.Terminate();
            return true;
        }
        #endregion
        public static void MakeNoise()
        {
            System.Media.SystemSounds.Beep.Play();
        }

        public static void InitializeConsole()
        {
            Console.ResetColor();
            Console.CursorVisible = false;
            SetConsoleCtrlHandler(Ctrl_C_Handler, true);
            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e) { //binds an event to the ctrl+c
                ResetConsole();
                Program.Terminate();
            };
        }

        internal static void InitializeHandlers()
        {
            
        }

        internal static HostPort GetTarget()
        {
            if (!Global.Parameters.TCP) return new HostPort(Global.Parameters.Target);
            if(Global.Parameters.Target.Contains(":")) {
                string[] sp = Global.Parameters.Target.Split(':');
                return new HostPort(sp[0], Int16.Parse(sp[1]));
            } else if(Global.Parameters.Port != 1) {
                return new HostPort(Global.Parameters.Target, (short)Global.Parameters.Port);
            } else {
                Console.WriteLine("Port missing! use <host>:<port> or the --port argument");
                Environment.Exit(0);
                return null;
            }
        }

        public static void InitializeParser(string[] args)
        {
            Global.Parser = new Parser(with =>
            {
                with.CaseSensitive = true;
                with.HelpWriter = Console.Error;
                with.EnableDashDash = true;
            });
            var result = Global.Parser.ParseArguments<Parameters>(args);
            result.WithParsed<Parameters>((res => Global.Parameters = res));
            result.WithNotParsed((err) => {
                var enumerator = err.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    Error error = enumerator.Current;
                    if (error.Tag == ErrorType.MissingRequiredOptionError)
                    {
                        Console.WriteLineFormatted("Missing target!", Color.Red);
                    } else
                    {
                        Console.WriteLine("Test!");
                    }

                }
                Environment.Exit(0);
            });
        }
        public static string GetProductVersion()
        {
            var attribute = (AssemblyVersionAttribute)Assembly
              .GetExecutingAssembly()
              .GetCustomAttributes(typeof(AssemblyVersionAttribute), true)
              .Single();
            return attribute.Version;
        }
        public static String MultiplyString(String str, int count)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < count; i++)
            {
                sb.Append(str);
            }
            return sb.ToString();
        }
        internal static void ResetConsole()
        {
            Console.CursorVisible = true;
            Console.ResetColor();
        }
    }
    public static class Extensions
    {
        public static void ForEachWithIndex<T>(this IEnumerable<T> enumerable, Action<T, int> handler)
        {
            int idx = 0;
            foreach (T item in enumerable)
                handler(item, idx++);
        }
    }
}