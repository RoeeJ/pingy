﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.NetworkInformation;
using System.Threading;
using System.Collections;
using System.IO;
using CommandLine;
using CommandLine.Text;
using PingyThingy.Pingers;
using Console = Colorful.Console;
using System.Drawing;
using Colorful;
using System.Diagnostics;

namespace PingyThingy
{
    class Program
    {
        static IPinger pinger;
        static void Main(string[] args)
        {
            Utils.InitializeConsole(); //Initializes console settings
            Utils.InitializeHandlers();
            Utils.InitializeParser(args); // Initializes the argument parser
            if (Global.Parameters.TCP) {
                pinger = new TCP();
            } else if (Global.Parameters.Trace) {
                pinger = new TraceRoute();
            } else {
                pinger = new ICMP();
            }
            try {
                pinger.Start();
            } catch(Exception ex) {
                Console.WriteLine(ex);
                Utils.ResetConsole();
                Console.ReadLine();
            }
        }

        internal static void Terminate()
        {
            pinger.Stop();
        }
    }
}
